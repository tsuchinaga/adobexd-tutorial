const {Selection, Path, Color} = require("scenegraph");
const commands = require("commands");

/**
 * @param {Selection} selection
 */
function createPieChartCommand(selection) {
    let artboard = selection.insertionParent;
    if ("addChild" in artboard) {
        // グループにするPathのリスト
        let paths = [];

        let wedge1 = createWedge(100, 0, 90, "red");
        paths.push(wedge1);
        artboard.addChild(wedge1);

        let wedge2 = createWedge(100, 90, 135, "blue");
        paths.push(wedge2);
        artboard.addChild(wedge2);

        let wedge3 = createWedge(100, 135, 225, "yellow");
        paths.push(wedge3);
        artboard.addChild(wedge3);

        let wedge4 = createWedge(100, 225, 360, "purple");
        paths.push(wedge4);
        artboard.addChild(wedge4);

        // グループ化
        selection.items = paths;
        commands.group();
    }
}

/**
 * @param {number} radius
 * @param {number} angle
 * @returns {string}
 */
function pointOnCircle(radius, angle) {
    const radians = angle * 2.0 * Math.PI / 360;
    return `${radius * Math.cos(radians)},${radius * Math.sin(radians)}`;
}

/**
 * @param radius
 * @param startAngle
 * @param endAngle
 * @param color
 * @return {Path} wedge
 */
function createWedge(radius, startAngle, endAngle, color) {
    const startPt = pointOnCircle(radius, startAngle);
    const endPt = pointOnCircle(radius, endAngle);
    const pathData = `M0,0 L${startPt} A${radius},${radius},0,0,1,${endPt} L0,0`;
    const wedge = new Path();
    wedge.pathData = pathData;
    wedge.fill = new Color(color);
    wedge.translate = {x: radius, y: radius};
    return wedge
}

module.exports = {
    commands: {
        "createPieChartCommand": createPieChartCommand
    }
};
