const {Artboard, Rectangle, Ellipse, Text, Color} = require("scenegraph");

/**
 * @param {Selection} selection
 */
function createElements(selection) {
    let board = selection.insertionParent;
    let grayColor = new Color("gray");

    for (let i = 0; i < 5; i++) {

        // draw rectangle
        let rect = new Rectangle();
        rect.width = 30 * i;
        rect.height = 20 * i;
        rect.fill = grayColor.clone();
        addChild(board, rect);
        rect.moveInParentCoordinates(50 * i, 50 * i);

        // draw ellipse
        let elli = new Ellipse();
        elli.radiusX = 20 * i;
        elli.radiusY = 20 * i;
        elli.fill = grayColor.clone();
        addChild(board, elli);
        elli.moveInParentCoordinates(100 * i, 200 * i);

        // draw text
        let text = new Text();
        text.text = `example text ${i}`;
        text.styleRanges = [
            {
                length: text.text.length,
                fill: grayColor.clone(),
                fontSize: 20,
            },
        ];
        addChild(board, text);
        text.moveInParentCoordinates(200 * i, 100 * i);
    }
}

/**
 * @param {Selection} selection
 * @param {RootNode} root
 */
function filterAndColor(selection, root) {
    root.children.forEach(node => {
        if (node instanceof Artboard) {
            let rectangles = node.children.filter(child => {
                return child instanceof Rectangle
            });
            rectangles.forEach(rect => {
                rect.fill = new Color("red")
            });
        }
    });
}

/**
 *
 * @param {SceneNode} board
 * @param {SceneNode} element
 */
function addChild(board, element) {
    if ("addChild" in board) {
        board.addChild(element)
    }
}

module.exports = {
    commands: {
        "createElements": createElements,
        "filterAndColor": filterAndColor,
    }
};
