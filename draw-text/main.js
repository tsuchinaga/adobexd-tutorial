const {Selection, Text, Color} = require("scenegraph");

/**
 * @param {Selection} selection
 */
function createTextsCommand(selection) {
    let artboard = selection.insertionParent;

    // 赤い文字
    const node = new Text();
    node.text = "新しくて赤いかっこいい文字";
    node.fill = new Color("red");
    node.fontSize = 24;

    if ("addChild" in artboard) {
        artboard.addChild(node);
        node.moveInParentCoordinates(20, 50);
    }

    // カラフルな文字
    const textData = [
        {text: "新しくて", color: "red"},
        {text: "カラフルで", color: "orange"},
        {text: "かっこよくて", color: "yellow"},
        {text: "きまってて", color: "green"},
        {text: "さわやかで", color: "blue"},
        {text: "自由な", color: "indigo"},
        {text: "文字", color: "violet"},
    ];
    const node2 = new Text();
    node2.text = textData.map(item => item.text).join("");
    node2.styleRanges = textData.map(item => ({
        length: item.text.length,
        fill: new Color(item.color)
    }));
    node2.fontSize = 24;

    if ("addChild" in artboard) {
        artboard.addChild(node2);
        node2.moveInParentCoordinates(20, 100);
    }
}

module.exports = {
    commands: {
        "createTextsCommand": createTextsCommand,
    }
};
